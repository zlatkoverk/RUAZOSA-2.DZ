package hr.fer.tel.ruazosa.lecture6.notes

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

class EnterNewNote : AppCompatActivity() {

    private var noteTitle: EditText? = null
    private var noteDescription: EditText? = null
    private var storeButton: Button? = null
    private var cancelButton: Button? = null
    private var oldTitle: String? = null
    private var oldDescription: String? = null
    private var errorView: TextView? = null

    var note = NotesModel.Note()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_new_note)

        noteTitle = findViewById(R.id.note_title)
        noteDescription = findViewById(R.id.note_description)
        errorView = findViewById(R.id.errorView)

        val extras = intent.extras
        if (extras != null) {
            noteTitle?.setText(extras.get("title").toString())
            noteDescription?.setText(extras.get("description").toString())
            oldTitle = extras.get("title").toString()
            oldDescription = extras.get("description").toString()
        }

        storeButton = findViewById(R.id.store_button)
        storeButton?.setOnClickListener({
            note = NotesModel.Note(noteTitle = noteTitle?.text.toString(),
                    noteDescription = noteDescription?.text.toString(), noteTime = Date())


            if (note.noteTitle.isNullOrEmpty()) {
                errorView?.text = "Please enter note title"
                return@setOnClickListener
            }
            if (note.noteDescription.isNullOrEmpty()) {
                errorView?.text = "Please enter note description"
                return@setOnClickListener
            }


            val result = Intent()

            result.putExtra("title", note.noteTitle)
            result.putExtra("description", note.noteDescription)
            if (oldTitle != null) {
                result.putExtra("oldTitle", oldTitle)
                result.putExtra("oldDescription", oldDescription)
            }

            setResult(0, result)
            finish()
        })
        cancelButton = findViewById(R.id.cancel_button)

        cancelButton?.setOnClickListener({
            setResult(1)
            finish()
        })
    }
}
