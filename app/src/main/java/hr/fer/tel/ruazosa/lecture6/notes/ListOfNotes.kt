package hr.fer.tel.ruazosa.lecture6.notes

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.j256.ormlite.stmt.PreparedDelete
import java.text.SimpleDateFormat
import java.util.*

class ListOfNotes : AppCompatActivity() {

    private var fab: FloatingActionButton? = null
    private var listView: ListView? = null
    private var notesAdapter: NotesAdapter? = null
    val newNoteRequest = 0
    val editNoteRequest = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_of_notes)
        fab = findViewById(R.id.fab)

        fab?.setOnClickListener({
            val startEnterNewNoteIntent = Intent(this, EnterNewNote::class.java)
            startActivityForResult(startEnterNewNoteIntent, newNoteRequest)
        })

        listView = findViewById(R.id.list_view)
        notesAdapter = NotesAdapter(this)
        listView?.adapter = notesAdapter
    }


    inner class NotesAdapter : BaseAdapter {

        private var notesList = NotesModel.notesList
        private var context: Context? = null


        constructor(context: Context) : super() {
            this.context = context
            val tableDao = DatabaseHelper(this@ListOfNotes).getDao(NotesModel.Note::class.java)
            NotesModel.notesList.addAll(tableDao.queryForAll())
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            val view: View?
            val vh: ViewHolder

            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.note_in_list, parent, false)
                vh = ViewHolder(view)
                view.tag = vh
            } else {
                view = convertView
                vh = view.tag as ViewHolder
            }
            val dateFormatter = SimpleDateFormat("dd'.'MM'.'yyyy ',' HH:mm");


            val dateAsString = dateFormatter.format(notesList[position].noteTime)
            vh.noteTitle.text = notesList[position].noteTitle
            vh.noteTime.text = dateAsString

            vh.noteDelete.setOnClickListener({
                var dao = DatabaseHelper(this@ListOfNotes).getDao(NotesModel.Note::class.java)

                var delBuilder = dao.deleteBuilder()
                delBuilder.where().eq("noteTitle", notesList[position].noteTitle).and().eq("noteTime", notesList[position].noteTime).and().eq("noteDescription", notesList[position].noteDescription);
                delBuilder.delete()

                this.notifyDataSetChanged()
                notesList.clear()
                notesList.addAll(dao.queryForAll())
            })

            vh.noteEdit.setOnClickListener({
                val startEnterNewNoteIntent = Intent(this@ListOfNotes, EnterNewNote::class.java)

                startEnterNewNoteIntent.putExtra("title", (getItem(position) as NotesModel.Note).noteTitle)
                startEnterNewNoteIntent.putExtra("description", (getItem(position) as NotesModel.Note).noteDescription)
                startActivityForResult(startEnterNewNoteIntent, 1)
            })
            return view
        }

        override fun getItem(position: Int): Any {
            return notesList[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return notesList.size
        }

        fun update() {
            var dao = DatabaseHelper(this@ListOfNotes).getDao(NotesModel.Note::class.java)
            notesList.clear()
            notesList.addAll(dao.queryForAll())
            notifyDataSetChanged()
        }
    }

    private class ViewHolder(view: View?) {
        val noteTime: TextView
        val noteTitle: TextView
        val noteEdit: ImageView
        val noteDelete: ImageView

        init {
            this.noteTime = view?.findViewById<TextView>(R.id.note_time) as TextView
            this.noteTitle = view?.findViewById<TextView>(R.id.note_title) as TextView
            this.noteEdit = view?.findViewById<ImageView>(R.id.ivEdit) as ImageView
            this.noteDelete = view?.findViewById<ImageView>(R.id.ivDelete) as ImageView
        }

        //  if you target API 26, you should change to:
//        init {
//            this.tvTitle = view?.findViewById<TextView>(R.id.tvTitle) as TextView
//            this.tvContent = view?.findViewById<TextView>(R.id.tvContent) as TextView
//        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        when (requestCode) {
            newNoteRequest -> {
                if (resultCode == 0) {
                    val note = NotesModel.Note()
                    val extras = data?.extras
                    if (extras != null) {
                        note.noteTitle = extras.get("title").toString()
                        note.noteDescription = extras.get("description").toString()
                        note.noteTime = Date()
                    }
                    val tableDao = DatabaseHelper(this).getDao(NotesModel.Note::class.java)
                    tableDao.create(note)

                    notesAdapter?.update()
                }

            }
            editNoteRequest -> {
                if (resultCode == 0) {
                    val note = NotesModel.Note()
                    val extras = data?.extras
                    if (extras != null) {
                        note.noteTitle = extras.get("title").toString()
                        note.noteDescription = extras.get("description").toString()
                        note.noteTime = Date()
                    }

                    var dao = DatabaseHelper(this@ListOfNotes).getDao(NotesModel.Note::class.java)

                    var delBuilder = dao.deleteBuilder()
                    delBuilder.where().eq("noteTitle", extras?.get("oldTitle")?.toString()).and().eq("noteDescription", extras?.get("oldDescription")?.toString())
                    delBuilder.delete()

                    dao.create(note)
                    notesAdapter?.update()
                }
            }
        }
    }
}
